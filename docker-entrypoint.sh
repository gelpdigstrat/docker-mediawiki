#!/bin/bash
set -euo pipefail

if [[ "$1" == apache2* ]] || [ "$1" == php-fpm ]; then

	: ${MEDIAWIKI_SITE_NAME:=MediaWiki}
	: ${MEDIAWIKI_SITE_LANG:=en}
	: ${MEDIAWIKI_ADMIN_USER:=admin}
	: ${MEDIAWIKI_ADMIN_PASS:=rosebud}
	: ${MEDIAWIKI_DB_TYPE:=mysql}
	: ${MEDIAWIKI_DB_SCHEMA:=mediawiki}
	: ${MEDIAWIKI_ENABLE_SSL:=false}
	: ${MEDIAWIKI_UPDATE:=false}
	: ${MEDIAWIKI_SCRIPT_PATH:=wiki}

	if ! [[ -v MEDIAWIKI_DB_HOST ]]; then
		if [[ -v MYSQL_PORT_3306_TCP_ADDR ]]; then
			MEDIAWIKI_DB_HOST=$MYSQL_PORT_3306_TCP_ADDR
		elif [[ -v POSTGRES_PORT_5432_TCP_ADDR ]]; then
			MEDIAWIKI_DB_TYPE=postgres
			MEDIAWIKI_DB_HOST=$POSTGRES_PORT_5432_TCP_ADDR
		elif [[ -v DB_PORT_3306_TCP_ADDR ]]; then
			MEDIAWIKI_DB_HOST=$DB_PORT_3306_TCP_ADDR
		elif [[ -v DB_PORT_5432_TCP_ADDR ]]; then
			MEDIAWIKI_DB_TYPE=postgres
			MEDIAWIKI_DB_HOST=$DB_PORT_5432_TCP_ADDR
		else
			echo >&2 'error: missing MEDIAWIKI_DB_HOST environment variable'
			echo >&2 '	Did you forget to --link your database?'
			exit 1
		fi
	fi

	if ! [[ -v MEDIAWIKI_DB_USER ]]; then
		if [ "$MEDIAWIKI_DB_TYPE" = "mysql" ]; then
			echo >&2 'info: missing MEDIAWIKI_DB_USER environment variable, defaulting to "root"'
			MEDIAWIKI_DB_USER=root
		elif [ "$MEDIAWIKI_DB_TYPE" = "postgres" ]; then
			echo >&2 'info: missing MEDIAWIKI_DB_USER environment variable, defaulting to "postgres"'
			MEDIAWIKI_DB_USER=postgres
		else
			echo >&2 'error: missing required MEDIAWIKI_DB_USER environment variable'
			exit 1
		fi
	fi

	if ! [[ -v MEDIAWIKI_DB_PASSWORD ]]; then
		if [[ -v MYSQL_ENV_MYSQL_ROOT_PASSWORD ]]; then
			MEDIAWIKI_DB_PASSWORD=$MYSQL_ENV_MYSQL_ROOT_PASSWORD
		elif [[ -v POSTGRES_ENV_POSTGRES_PASSWORD ]]; then
			MEDIAWIKI_DB_PASSWORD=$POSTGRES_ENV_POSTGRES_PASSWORD
		elif [[ -v DB_ENV_MYSQL_ROOT_PASSWORD ]]; then
			MEDIAWIKI_DB_PASSWORD=$DB_ENV_MYSQL_ROOT_PASSWORD
		elif [[ -v DB_ENV_POSTGRES_PASSWORD ]]; then
			MEDIAWIKI_DB_PASSWORD=$DB_ENV_POSTGRES_PASSWORD
		else
			echo >&2 'error: missing required MEDIAWIKI_DB_PASSWORD environment variable'
			echo >&2 '	Did you forget to -e MEDIAWIKI_DB_PASSWORD=... ?'
			echo >&2
			echo >&2 '	(Also of interest might be MEDIAWIKI_DB_USER and MEDIAWIKI_DB_NAME)'
			exit 1
		fi
	fi

	: ${MEDIAWIKI_DB_NAME:=mediawiki}

	if ! [[ -v MEDIAWIKI_DB_PORT ]]; then
		if [[ -v MYSQL_PORT_3306_TCP_PORT ]]; then
			MEDIAWIKI_DB_PORT=$MYSQL_PORT_3306_TCP_PORT
		elif [[ -v POSTGRES_PORT_5432_TCP_PORT ]]; then
			MEDIAWIKI_DB_PORT=$POSTGRES_PORT_5432_TCP_PORT
		elif [[ -v DB_PORT_3306_TCP_PORT ]]; then
			MEDIAWIKI_DB_PORT=$DB_PORT_3306_TCP_PORT
		elif [[ -v DB_PORT_5432_TCP_PORT ]]; then
			MEDIAWIKI_DB_PORT=$DB_PORT_5432_TCP_PORT
		elif [ "$MEDIAWIKI_DB_TYPE" = "mysql" ]; then
			MEDIAWIKI_DB_PORT="3306"
		elif [ "$MEDIAWIKI_DB_TYPE" = "postgres" ]; then
			MEDIAWIKI_DB_PORT="5432"
		fi
	fi

	# Wait for the DB to come up
	while [ `/bin/nc -w 1 $MEDIAWIKI_DB_HOST $MEDIAWIKI_DB_PORT < /dev/null > /dev/null; echo $?` != 0 ]; do
	    echo "Waiting for database to come up at $MEDIAWIKI_DB_HOST:$MEDIAWIKI_DB_PORT..."
	    sleep 1
	done

	export MEDIAWIKI_DB_TYPE MEDIAWIKI_DB_HOST MEDIAWIKI_DB_USER MEDIAWIKI_DB_PASSWORD MEDIAWIKI_DB_NAME MEDIAWIKI_SCRIPT_PATH

	TERM=dumb php -- <<'EOPHP'
	<?php
	// database might not exist, so let's try creating it (just to be safe)
	if ($_ENV['MEDIAWIKI_DB_TYPE'] == 'mysql') {
		$mysql = new mysqli($_ENV['MEDIAWIKI_DB_HOST'], $_ENV['MEDIAWIKI_DB_USER'], $_ENV['MEDIAWIKI_DB_PASSWORD'], '', (int) $_ENV['MEDIAWIKI_DB_PORT']);
		if ($mysql->connect_error) {
			file_put_contents('php://stderr', 'MySQL Connection Error: (' . $mysql->connect_errno . ') ' . $mysql->connect_error . "\n");
			exit(1);
		}
		if (!$mysql->query('CREATE DATABASE IF NOT EXISTS `' . $mysql->real_escape_string($_ENV['MEDIAWIKI_DB_NAME']) . '`')) {
			file_put_contents('php://stderr', 'MySQL "CREATE DATABASE" Error: ' . $mysql->error . "\n");
			$mysql->close();
			exit(1);
		}
		$mysql->close();
	}
EOPHP

	cd /var/www/html/wiki


	# Attempt to enable SSL support if explicitly requested
	if [ $MEDIAWIKI_ENABLE_SSL = true ]; then
		echo >&2 'info: enabling ssl'
		a2enmod ssl

	elif [ -e "/etc/apache2/mods-enabled/ssl.load" ]; then
		echo >&2 'warning: disabling ssl'
		a2dismod ssl
	fi

	# If there is no LocalSettings.php, create one using maintenance/install.php
	if [ ! -e "LocalSettings.php" -a ! -z "$MEDIAWIKI_SITE_SERVER" ]; then
		php maintenance/install.php \
			--confpath /var/www/html/wiki \
			--dbname "$MEDIAWIKI_DB_NAME" \
			--dbschema "$MEDIAWIKI_DB_SCHEMA" \
			--dbport "$MEDIAWIKI_DB_PORT" \
			--dbserver "$MEDIAWIKI_DB_HOST" \
			--dbtype "$MEDIAWIKI_DB_TYPE" \
			--dbuser "$MEDIAWIKI_DB_USER" \
			--dbpass "$MEDIAWIKI_DB_PASSWORD" \
			--installdbuser "$MEDIAWIKI_DB_USER" \
			--installdbpass "$MEDIAWIKI_DB_PASSWORD" \
			--server "$MEDIAWIKI_SITE_SERVER" \
			--scriptpath "/$MEDIAWIKI_SCRIPT_PATH" \
			--lang "$MEDIAWIKI_SITE_LANG" \
			--pass "$MEDIAWIKI_ADMIN_PASS" \
			"$MEDIAWIKI_SITE_NAME" \
			"$MEDIAWIKI_ADMIN_USER"

	fi

	# If LocalSettings.php exists, then attempt to run the update.php maintenance
	# script. If already up to date, it won't do anything, otherwise it will
	# migrate the database if necessary on container startup. It also will
	# verify the database connection is working.
	if [ -e "LocalSettings.php" -a "$MEDIAWIKI_UPDATE" = 'true' ]; then
		echo >&2 'info: Running maintenance/update.php';
		php maintenance/update.php --quick --conf ./LocalSettings.php
	fi

fi

echo Sarting...

exec "$@"
