# docker-mediawiki

## How to use this image

```
docker run --name mywiki -d telusgelp/mediawiki
```
## Extra Configuration options

Use the following environment variables to generate a `LocalSettings.php` and install mediawiki.

* **MEDIAWIKI_SITE_SERVER** - set to server host eg http://localhost:8080
* **MEDIAWIKI_SITE_NAME** - defualts to `MediaWiki`
* **MEDIAWIKI_SITE_LANG** - defaults to `en`
* **MEDIAWIKI_ADMIN_USER** - defaults to `admin`
* **MEDIAWIKI_ADMIN_PASS** - defaults to `rosebud`
* **MEDIAWIKI_DB_TYPE** - defaults to `mysql`
* **MEDIAWIKI_DB_SCHEMA** - name of database. Defaults to `mediawiki`
* **MEDIAWIKI_ENABLE_SSL** - defaults to `false`
* **MEDIAWIKI_UPDATE** - Whether to update database schema if wordpress updates. Defaults to `false`.
* **MEDIAWIKI_SCRIPT_PATH** - base URL path. Defaults to `wiki`
